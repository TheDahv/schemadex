package crawler

import (
	"bytes"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"sync"
	"testing"

	"gitlab.com/thedahv/schemadex/pkg/schema"
)

func TestLinkCrawling(t *testing.T) {
	cwd, err := os.Getwd()
	if err != nil {
		t.Errorf("unable to get current directory: %v", err)
		t.FailNow()
	}

	tt := []struct {
		name string
		dir  string
	}{
		{
			name: "sites with circular links",
			dir:  "circular",
		},
		{
			name: "sites with common repeated links ",
			dir:  "common-links",
		},
	}

	reqTracking := make(map[string]int)
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {

			dir := http.Dir(cwd + "/testdata/links/" + tc.dir)

			// Clear out request tracking for every run
			for entry := range reqTracking {
				delete(reqTracking, entry)
			}

			addr, teardown := setupServer(t, tc.dir, dir, reqTracking)
			defer teardown()

			if err != nil {
				t.Errorf("error setting up file server: %v", err)
				t.FailNow()
			}

			src, err := url.Parse("http://" + addr.String() + "/" + tc.dir + "/root.html")
			if err != nil {
				t.Errorf("error setting up root URL: %v", err)
				t.FailNow()
			}
			d, _ := New(*src)
			schema, errors := d.Start()

			var schemaCount int
			var errorsCount int
			for {
				select {
				case _, ok := <-schema:
					if !ok {
						schema = nil
						break
					}
					schemaCount++
				case e, ok := <-errors:
					if !ok {
						errors = nil
						break
					}
					errorsCount++
					t.Errorf("crawl error: %v", e)
				}
				if schema == nil && errors == nil {
					break
				}
			}

			if schemaCount != 0 {
				t.Errorf("got %d schema, expected %d", schemaCount, 0)
			}
			if errorsCount != 0 {
				t.Errorf("got %d errors, expected %d", errorsCount, 0)
			}

			for path, count := range reqTracking {
				if count != 1 {
					t.Errorf("got %d visits for '%s', expected %d",
						count, path, 1)
				}
			}
		})
	}
}

func TestLinkFilters(t *testing.T) {
	tt := []struct {
		name       string
		sourceURL  string
		testURL    string
		expected   bool
		linkFilter func(t *testing.T) LinkFilter
	}{
		{
			name:      "internal-only link filter with external link",
			sourceURL: "http://www.somesite.com",
			testURL:   "http://sub.somesite.com",
			expected:  false,
			linkFilter: func(t *testing.T) LinkFilter {
				src, err := url.Parse("http://www.somesite.com")
				if err != nil {
					t.Errorf("couldn't set up source URL: %v", err)
					t.FailNow()
				}
				return FilterInternalOnly(*src)
			},
		},
		{
			name:      "internal-only link filter with internal link",
			sourceURL: "http://www.somesite.com",
			testURL:   "http://www.somesite.com/something",
			expected:  true,
			linkFilter: func(t *testing.T) LinkFilter {
				src, err := url.Parse("http://www.somesite.com")
				if err != nil {
					t.Errorf("couldn't set up source URL: %v", err)
					t.FailNow()
				}
				return FilterInternalOnly(*src)
			},
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			lf := tc.linkFilter(t)
			testURL, err := url.Parse(tc.testURL)
			if err != nil {
				t.Errorf("couldn't set up test URL: %v", err)
				t.FailNow()
			}

			if actual := lf(*testURL); actual != tc.expected {
				t.Errorf("link filter evaluated %v, expected %v", actual, tc.expected)
			}
		})
	}
}

func TestLinkReader(t *testing.T) {
	tt := []struct {
		name           string
		body           string
		expectedLinks  int
		expectedErrors int
	}{
		{
			name: "normal page",
			body: `
			<html>
				<body>
					<a href="/link">Link</a>
					<a href="/link-two">Link</a>
					<a href="/link-three">Link</a>
					<a href="/link-four">Link</a>
				</body>
			</html>`,
			expectedLinks:  4,
			expectedErrors: 0,
		},
	}

	src, err := url.Parse("http://www.website.com")
	if err != nil {
		t.Errorf("error setting up test URL: %v", err)
		t.FailNow()
	}
	d, _ := New(*src)

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			linkCapture := make(chan url.URL)
			errorCapture := make(chan error)
			d.links = linkCapture
			d.errors = errorCapture

			var wg sync.WaitGroup
			wg.Add(2)

			var errorCount int
			go func() {
				for range d.errors {
					errorCount++
				}
				wg.Done()
			}()

			var links []url.URL
			go func() {
				for l := range linkCapture {
					links = append(links, l)
				}
				wg.Done()
			}()

			body := bytes.NewReader([]byte(tc.body))
			d.linkReader(*src, body)

			close(errorCapture)
			close(linkCapture)
			wg.Wait()

			if actual := len(links); actual != tc.expectedLinks {
				t.Errorf("got %d links, expected %d", actual, tc.expectedLinks)
			}

			if errorCount != tc.expectedErrors {
				t.Errorf("got %d errors, expected %d", errorCount, tc.expectedErrors)
			}
		})
	}
}

func TestSchemaReader(t *testing.T) {
	tt := []struct {
		name           string
		body           string
		expectedSchema schema.Schema
	}{
		{
			name: "normal page with microdata",
			body: `
			<html>
				<body>
					<div itemscope itemtype="http://schema.org/Test">
					</div>
				</body>
			</html>`,
			expectedSchema: schema.Schema{
				Type:           "Test",
				Implementation: schema.Microdata,
				VocabularyHost: "schema.org",
			},
		},
		{
			name: "normal page with json+ld",
			body: `
			<html>
				<body>
					<script type="application/ld+json">
						{
							"@context": "http://schema.org",
							"@type": "Test"
						}
					</script>
				</body>
			</html>`,
			expectedSchema: schema.Schema{
				Type:           "Test",
				Implementation: schema.JSONLD,
				VocabularyHost: "schema.org",
			},
		},
	}

	src, err := url.Parse("http://www.website.com")
	if err != nil {
		t.Errorf("error setting up test URL: %v", err)
		t.FailNow()
	}
	d, _ := New(*src)

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			schemaCapture := make(chan schema.Schema)
			d.schemas = schemaCapture

			body := bytes.NewReader([]byte(tc.body))
			go d.schemaReader(*src, body)
			schema := <-schemaCapture
			close(schemaCapture)

			if schema.Type != tc.expectedSchema.Type {
				t.Errorf("got type %s, expected %s",
					schema.Type, tc.expectedSchema.Type)
			}

			if schema.Implementation != tc.expectedSchema.Implementation {
				t.Errorf("got implementation %s, expected %s",
					schema.Implementation, tc.expectedSchema.Implementation)
			}

			if schema.VocabularyHost != tc.expectedSchema.VocabularyHost {
				t.Errorf("got vocabulary host %s, expected %s",
					schema.VocabularyHost, tc.expectedSchema.VocabularyHost)
			}
		})
	}
}

type serverTeardown func()

func setupServer(
	t *testing.T,
	root string,
	dir http.Dir,
	requestTracking map[string]int,
) (net.Addr, serverTeardown) {

	t.Helper()
	listener, err := net.Listen("tcp", ":0")
	if err != nil {
		t.Errorf("cannot set up listener: %v", err)
		t.FailNow()
	}

	fs := http.FileServer(dir)
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		requestTracking[r.URL.Path]++
		fs.ServeHTTP(w, r)
	})
	http.HandleFunc("/"+root+"/", handler)

	go func() {
		log.Fatal(http.Serve(listener, nil))
	}()

	return listener.Addr(), func() {
		log.Fatal(listener.Close())
	}
}
