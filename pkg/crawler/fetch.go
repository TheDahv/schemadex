package crawler

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"sync"

	"gitlab.com/thedahv/schemadex/pkg/schema"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

// Driver runs a crawl configured against a given source URL. Link URLs meeting
// the given criteria are emitted on the Links channel.
type Driver struct {
	concurrency int
	client      Client
	source      url.URL
	linkFilter  LinkFilter

	lq          *NewQueueSet
	errors      chan error
	links       chan url.URL
	schemas     chan schema.Schema
	pageCrawled chan struct{}
	done        chan struct{}
}

// New returns a new driver ready to crawl against the given source URL with the
// given configuration options applied.
func New(source url.URL, options ...Option) (*Driver, error) {
	d := &Driver{
		lq:          NewQueue(),
		errors:      make(chan error),
		links:       make(chan url.URL),
		schemas:     make(chan schema.Schema),
		source:      source,
		pageCrawled: make(chan struct{}),
		done:        make(chan struct{}, 1),
	}

	for _, opt := range options {
		if err := opt(d); err != nil {
			return d, err
		}
	}

	if d.client == nil {
		d.client = &http.Client{}
	}

	return d, nil
}

// Option is a pre-defined operation to configure a driver
type Option func(*Driver) error

// WithClient configures the driver witha  specific client. If not configured,
// the default http.Client is used. The client must be safe for concurrent use
// by multiple goroutines and should support being reused multiple times.
var WithClient = func(c Client) Option {
	return func(d *Driver) error {
		d.client = c
		return nil
	}
}

// WithConcurrency configures how many crawler clients to run concurrencly while
// crawling a site.
var WithConcurrency = func(concurrency int) Option {
	return func(d *Driver) error {
		d.concurrency = concurrency
		return nil
	}
}

// WithFilter creates an option to filter crawlable links that meet criteria as
// indicated by return value of the LinkFilter function.
var WithFilter = func(lf LinkFilter) Option {
	return func(d *Driver) error {
		d.linkFilter = lf
		return nil
	}
}

// LinkFilter are functions that indicate whether the URL for a link matches
// crawl criteria.
type LinkFilter func(url.URL) bool

// FilterCompose composes many link filters into a serial chain. Order matters.
// Links are evaluated by those earlier in the chain first.
var FilterCompose = func(filters ...LinkFilter) LinkFilter {
	return func(url url.URL) bool {
		for _, lf := range filters {
			if !lf(url) {
				return false
			}
		}

		return true
	}
}

// FilterInternalOnly is a common filter function limiting crawled links to
// those internal to a site as indicated by the sourceURL argument.
var FilterInternalOnly = func(sourceURL url.URL) LinkFilter {
	return func(url url.URL) bool {
		return url.Hostname() == sourceURL.Hostname()
	}
}

// FilterHTTPOnly is a common filter function to limit links to http or https
// links, discarding other kinds of links like 'tel', etc.
var FilterHTTPOnly = func(url url.URL) bool {
	return url.Scheme == "http" || url.Scheme == "https"
}

// Client is an interface that can fetch HTML contents from a remote source. The
// Client from the HTTP package implements this interface.
type Client interface {
	Get(string) (*http.Response, error)
}

// Start begins a crawl with the configured number of workers consuming new URLs
// from the link queue. The crawl ends when the link queue is empty after the
// last page indicates it is done crawling.
//
// Any schema encountered is emitted on the Schema channel, as well as any
// errors. The channels will close when the crawl is complete.
func (d *Driver) Start() (chan schema.Schema, chan error) {
	final := make(chan struct{}, 1)
	conc := d.concurrency
	if conc == 0 {
		conc = 1
	}

	go func() {
		for i := 0; i < conc; i++ {
			go func() {
			Loop:
				for {
					select {
					case l := <-d.links:
						d.lq.Add(l)
					case <-d.pageCrawled:
						if d.lq.Empty() {
							d.done <- struct{}{}
						} else {
							go d.Fetch()
						}
					case <-d.done:
						break Loop
					}
				}

				final <- struct{}{}
			}()
		}

		d.lq.Add(d.source)
		d.Fetch()

		<-final

		close(d.schemas)
		close(d.errors)
	}()

	return d.schemas, d.errors
}

// Fetch loads the source page from the remote source, crawls its contents, and
// returns a slice of fully-qualified link URLs it finds on the page.  Internal
// links (that is, those without a protocol or domain are rebuilt based on the
// context of the page URL at which they were found).
func (d *Driver) Fetch() {
	log.SetPrefix("[Crawler/Fetch] ")
	source := d.lq.Dequeue()

	log.Println("Crawling", source.String())
	resp, err := d.client.Get(source.String())
	if err != nil {
		// Not being able to fetch a page is a bail-worthy event, so we quit here
		// Our crawler will hang if we don't declare the page crawled so we emit
		// that too.
		d.errors <- err
		d.pageCrawled <- struct{}{}
		return
	}

	// The HTTP response body stream is forked to 2 readers in parallel so we can
	// scan for links and structured schema data in a single pass. We use io.Pipe
	// to create a writer for each stream consumer, io.Multiwriter to multiplex
	// the HTTP stream to the pipe readers, and io.Copy to start consuming the
	// stream.
	linkR, linkW := io.Pipe()
	schemaR, schemaW := io.Pipe()

	var wg sync.WaitGroup
	wg.Add(2)

	go func() {
		d.linkReader(source, linkR)
		wg.Done()
	}()
	go func() {
		d.schemaReader(source, schemaR)
		wg.Done()
	}()

	go func() {
		mw := io.MultiWriter(linkW, schemaW)
		_, err := io.Copy(mw, resp.Body)
		if err != nil {
			d.errors <- fmt.Errorf("error streaming HTTP response body: %v", err)
		}

		// TODO handle all of these...not sure how yet
		if err := linkW.Close(); err != nil {
			d.errors <- fmt.Errorf("error closing link streamer: %v", err)
		}
		if err := schemaW.Close(); err != nil {
			d.errors <- fmt.Errorf("error closing schema streamer: %v", err)
		}
		if err := resp.Body.Close(); err != nil {
			d.errors <- fmt.Errorf("error closing HTTP response body: %v", err)
		}
	}()

	wg.Wait()
	d.pageCrawled <- struct{}{}
}

// Depth reports the number of links still in the crawl queue
func (d *Driver) Depth() int {
	return d.lq.Length()
}

// linkReader finds links in an HTML page and emits their URLs on the driver's
// links channel.
func (d *Driver) linkReader(pageURL url.URL, source io.Reader) {
	z := html.NewTokenizer(source)

	for {
		tt := z.Next()
		if tt == html.ErrorToken {
			if z.Err() != io.EOF {
				// this happens when the tokenizer can't evaluate the HTML. I suppose
				// that means we should be done with this page
				d.errors <- fmt.Errorf("issue parsing HTML for links: %v", z.Err())
			}
			return
		}

		switch tt {
		case html.StartTagToken:
			tok := z.Token()
			if tok.DataAtom == atom.A {
				href, ok := getHref(tok)
				// Skip bad links and those an anchor on the same page
				if !ok || href == "" || href[0] == '#' {
					continue
				}

				parsed, err := url.Parse(href)
				// If we can't parse the URL, just skip it. This is a weird link
				if err != nil {
					d.errors <- fmt.Errorf("unable to parse URL: %v (%s)", err, href)
					continue
				}

				if !parsed.IsAbs() {
					parsed.Scheme = d.source.Scheme
				}
				if parsed.Hostname() == "" {
					parsed.Host = d.source.Host
				}

				if d.linkFilter == nil || d.linkFilter(*parsed) {
					d.links <- *parsed
				}
			}
		}
	}
}

// schemaReader finds any schema data, whether encoded in JSON-LD or microdata,
// normalizes the usage into a common format, and emits that on the driver.
func (d *Driver) schemaReader(pageURL url.URL, source io.Reader) {
	z := html.NewTokenizer(source)

	for {
		tt := z.Next()
		if tt == html.ErrorToken {
			if z.Err() != io.EOF {
				d.errors <- fmt.Errorf(
					"issue parsing HTML for JSON content: %v",
					z.Err(),
				)
			}
			return
		}

		switch tt {
		case html.StartTagToken:
			tok := z.Token()
			if tok.DataAtom == atom.Script && isSchemaJSON(tok) {
				// Advance token to get raw text child
				z.Next()
				r := bytes.NewReader(z.Raw())
				// TODO if the content of the script tag is empty, we may need to
				// download the JSON from the remote source.
				parseJSONSchema(pageURL, r, d.schemas, d.errors)
			} else if isMicrodata(tok) {
				parseMicrodata(pageURL, tok, d.schemas, d.errors)
			}
		}
	}
}

func getHref(link html.Token) (string, bool) {
	return getAttr(link, "href")
}

func getAttr(tok html.Token, key string) (string, bool) {
	for _, attr := range tok.Attr {
		if attr.Key == key {
			return attr.Val, true
		}
	}

	return "", false
}
