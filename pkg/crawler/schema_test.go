package crawler

import (
	"bytes"
	"net/url"
	"sync"
	"testing"

	"gitlab.com/thedahv/schemadex/pkg/schema"
)

func TestParseJSONSchema(t *testing.T) {
	tt := []struct {
		name     string
		body     string
		expected []schema.Schema
		errors   int
	}{
		{
			name: "normal document",
			body: `{
				"@context": "http://schema.org",
				"@type": "Test",
				"suiteName": "TestParseJSONSchema",
				"caseName": "normal document"
			}`,
			expected: []schema.Schema{
				{
					Type:           "Test",
					Implementation: schema.JSONLD,
					VocabularyHost: "schema.org",
					PageURL:        "http://www.test.com/test",
					DomainURL:      "www.test.com",
				},
			},
			errors: 0,
		},
		{
			name: "malformed document with no possible schema",
			body: `{
				@context: "http://schema.org",
				@type: "Test",
				suiteName: "TestParseJSONSchema",
				caseName: "normal document"
			}`,
			expected: []schema.Schema{},
			errors:   1,
		},
		{
			name: "malformed document with some possible schema",
			body: `{
				"@context": "http://schema.org",
				"@type": "Test",
				"suiteName": "TestParseJSONSchema",
				"caseName": "normal document"
			};`,
			expected: []schema.Schema{
				{
					Type:           "Test",
					Implementation: schema.JSONLD,
					VocabularyHost: "schema.org",
					PageURL:        "http://www.test.com/test",
					DomainURL:      "www.test.com",
				},
			},
			errors: 1,
		},
	}

	for _, tc := range tt {
		pageURL, err := url.Parse("http://www.test.com/test")
		if err != nil {
			t.Errorf("unable to set up test URL: %v", err)
			t.FailNow()
		}

		t.Run(tc.name, func(t *testing.T) {
			src := bytes.NewReader([]byte(tc.body))
			var schemas []schema.Schema
			var errors []error
			schemaCapture := make(chan schema.Schema)
			errorCapture := make(chan error)

			var wg sync.WaitGroup
			wg.Add(2)
			go func() {
				for s := range schemaCapture {
					schemas = append(schemas, s)
				}
				wg.Done()
			}()
			go func() {
				for e := range errorCapture {
					errors = append(errors, e)
				}
				wg.Done()
			}()

			parseJSONSchema(*pageURL, src, schemaCapture, errorCapture)

			close(schemaCapture)
			close(errorCapture)
			wg.Wait()

			if actual := len(schemas); actual != len(tc.expected) {
				t.Errorf("got %d schemas, expected %d", actual, len(tc.expected))
			}
			if actual := len(errors); actual != len(errors) {
				t.Errorf("got %d errors, expected %d", actual, len(errors))
			}
			for i, schema := range schemas {
				if schema.Type != tc.expected[i].Type {
					t.Errorf("result %d: got Type %s, expected %s",
						i, schema.Type, tc.expected[i].Type)
				}
				if schema.Implementation != tc.expected[i].Implementation {
					t.Errorf("result %d: got Implementation %s, expected %s",
						i, schema.Implementation, tc.expected[i].Implementation)
				}
				if schema.VocabularyHost != tc.expected[i].VocabularyHost {
					t.Errorf("result %d: got VocabularyHost %s , expected %s",
						i, schema.VocabularyHost, tc.expected[i].VocabularyHost)
				}
				if schema.PageURL != tc.expected[i].PageURL {
					t.Errorf("result %d: got PageURL %s, expected %s",
						i, schema.PageURL, tc.expected[i].PageURL)
				}
				if schema.DomainURL != tc.expected[i].DomainURL {
					t.Errorf("result %d: got DomainURL %s, expected %s",
						i, schema.DomainURL, tc.expected[i].DomainURL)
				}
			}
		})
	}
}
