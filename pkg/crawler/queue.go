package crawler

import (
	"net/url"
	"sync"
)

// NewQueueSet is a concurrency-ready queue which tracks links that need to be
// processed during a crawl. It retains memory of links already visited and
// prevents them from being added to the queue again.
type NewQueueSet struct {
	q    []url.URL
	lock sync.RWMutex
	set  map[string]bool
}

// NewQueue creates a new queue.
func NewQueue() *NewQueueSet {
	return &NewQueueSet{
		q:   []url.URL{},
		set: make(map[string]bool),
	}
}

// Empty returns true if there are no items in the queue.
func (lq *NewQueueSet) Empty() bool {
	lq.lock.RLock()
	defer lq.lock.RUnlock()

	return len(lq.q) == 0
}

// Length returns the number of items remaining in the queue.
func (lq *NewQueueSet) Length() int {
	lq.lock.RLock()
	defer lq.lock.RUnlock()

	return len(lq.q)
}

// Add adds a new item to the queue if it has not beeen seen before.
func (lq *NewQueueSet) Add(item url.URL) {
	lq.lock.Lock()
	if seen := lq.set[item.String()]; !seen {
		lq.q = append(lq.q, item)
		lq.set[item.String()] = true
	}
	lq.lock.Unlock()
}

// AddMany adds multiple items to the queue at once, filtering out any entries
// that have previously been seen before.
func (lq *NewQueueSet) AddMany(items []url.URL) {
	lq.lock.Lock()
	var new []url.URL
	for _, i := range items {
		if seen := lq.set[i.String()]; !seen {
			new = append(new, i)
		}
	}

	lq.q = append(lq.q, new...)
	lq.lock.Unlock()
}

// Dequeue removes the first item in the queue and returns it. The item's entry
// in the map of visited links remains.
func (lq *NewQueueSet) Dequeue() url.URL {
	lq.lock.Lock()
	defer lq.lock.Unlock()

	if len(lq.q) == 0 {
		return url.URL{}
	}

	item := lq.q[0]
	lq.q = lq.q[1:]

	return item
}
