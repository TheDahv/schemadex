package crawler

import (
	"fmt"
	"net/url"
	"sync"
	"testing"
)

func TestConcurrentAccess(t *testing.T) {
	queue := NewQueue()

	numEntries := 100
	for i := 0; i < numEntries; i++ {
		u, _ := url.Parse(fmt.Sprintf("http://www.test.com/%d", i))
		queue.Add(*u)
	}

	if l := queue.Length(); l != numEntries {
		t.Errorf("got %d items in the queue, expected %d", l, numEntries)
	}

	groups := 5
	var wg sync.WaitGroup
	wg.Add(5)
	for i := 0; i < 5; i++ {
		go func() {
			for i := 0; i < numEntries/groups; i++ {
				queue.Dequeue()
			}
			wg.Done()
		}()
	}

	wg.Wait()
	if l := queue.Length(); l != 0 {
		t.Errorf("got %d items remaining in queue, expected %d", l, 0)
	}

	if e := queue.Empty(); e != true {
		t.Error("got non-empty queue, expected empty")
	}
}

func TestAddMany(t *testing.T) {
	queue := NewQueue()

	var urls []url.URL
	numEntries := 100
	for i := 0; i < numEntries; i++ {
		u, _ := url.Parse(fmt.Sprintf("http://www.test.com/%d", i))
		urls = append(urls, *u)
	}

	queue.AddMany(urls)

	if l := queue.Length(); l != numEntries {
		t.Errorf("got %d entries in queue, expected %d", l, numEntries)
	}
}

func TestSetUniqueness(t *testing.T) {
	queue := NewQueue()
	u, _ := url.Parse("http://www.test.com")
	for i := 0; i < 100; i++ {
		queue.Add(*u)
	}

	if l := queue.Length(); l != 1 {
		t.Errorf("got %d entries, expected 1", l)
	}
}

func TestEmptyQueue(t *testing.T) {
	queue := NewQueue()
	if e := queue.Empty(); e != true {
		t.Errorf("got non-empty queue, expected empty")
	}

	u := queue.Dequeue()
	if u := u.String(); u != "" {
		t.Errorf("got '%s', expected empty URL", u)
	}
}
