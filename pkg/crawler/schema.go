package crawler

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/url"
	"time"

	"gitlab.com/thedahv/schemadex/pkg/schema"
	"golang.org/x/net/html"
)

func isSchemaJSON(tok html.Token) bool {
	attr, ok := getAttr(tok, "type")
	return ok && attr == "application/ld+json"
}

// parseJSONSchema takes in a reader for the bytes making up a JSON payload and
// extracts Schema information from it. It walks through the JSON tokens looking
// for "@type" keys which designate the schema.org vocabulary type for an entry.
//
// The context for a JSON+LD document is usually declared once as a top-level
// key. If the function encounteres the key, it stores it and associates all
// further types with this context.
//
// Any schema or error values encountered during processing are emitted on the
// channels passed in from the calling code. This function does not attempt to
// close the channels when it is finished.
func parseJSONSchema(pageURL url.URL, data io.Reader, schemas chan schema.Schema, errors chan error) {
	log.SetPrefix("[JSON Parser] ")

	d := json.NewDecoder(data)
	var context string

	for {
		key, err := d.Token()
		if err != nil {
			if err != io.EOF {
				// TODO in this case, the JSON has a trailing semicolon which breaks most
				// parsers. In our case, we should grab as much as we can and then bail, I
				// suppose.
				// https://www.allrecipes.com/recipes/76/appetizers-and-snacks/
				errors <- fmt.Errorf("error reading starting token: %v", err)
			}
			return
		}

		switch key.(type) {
		case string:
			if key, ok := key.(string); ok {
				switch key {
				case "@context":
					ctx, err := d.Token()
					if err != nil {
						if err != io.EOF {
							errors <- fmt.Errorf("non-EOF error reading context token: %v", err)
						}
						return
					}

					if ctx, ok := ctx.(string); ok {
						ctxURL, err := url.Parse(ctx)
						if err != nil {
							errors <- fmt.Errorf("unable to parse schema host: %s", err)
						}
						context = ctxURL.Hostname()
					}

				case "@type":
					val, err := d.Token()
					if err != nil {
						if err != io.EOF {
							errors <- fmt.Errorf("non-EOF error reading type token: %v", err)
						}
						return
					}

					if val, ok := val.(string); ok {
						schemas <- schema.Schema{
							Type:           val,
							Implementation: schema.JSONLD,
							VocabularyHost: context,
							PageURL:        pageURL.String(),
							DomainURL:      pageURL.Hostname(),
							CrawledAt:      time.Now(),
						}
					}
				}
			}
		}
	}
}

func isMicrodata(tok html.Token) bool {
	_, itemscopeOk := getAttr(tok, "itemscope")
	_, itemtypeOk := getAttr(tok, "itemtype")

	return itemscopeOk && itemtypeOk
}

// parseMicrodata extracts Schema.org from microdata attributes on a given HTML
// node. The type is usually marked up as a fully-qualified URL to the schema
// type definition in a single "itemtype" attribute in the HTML node. This value
// is split into respective parts and the resulting value emitted on the schema
// channel. Any errors encountered are emitted on the errors channel passed in.
// The function does not attempt to close either channel when finished.
func parseMicrodata(pageURL url.URL, tok html.Token, schemas chan schema.Schema, errors chan error) {
	log.SetPrefix("[Microdata Parser] ")
	itemtype, ok := getAttr(tok, "itemtype")
	if !ok {
		return
	}

	vocabHost, schemaType, err := splitSchemaURL(itemtype)
	if err != nil {
		errors <- fmt.Errorf("error splitting itemtype URL: %v", err)
		return
	}

	schemas <- schema.Schema{
		Type:           schemaType,
		Implementation: schema.Microdata,
		VocabularyHost: vocabHost,
		PageURL:        pageURL.String(),
		DomainURL:      pageURL.Hostname(),
		CrawledAt:      time.Now(),
	}
}

func splitSchemaURL(path string) (string, string, error) {
	parsed, err := url.Parse(path)
	if err != nil {
		return "", "", err
	}

	schemaType := parsed.Path
	if schemaType[0] == '/' {
		schemaType = schemaType[1:]
	}

	return parsed.Hostname(), schemaType, nil
}
