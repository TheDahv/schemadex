package manager

import (
	"fmt"
	"log"
	"net/url"
	"os"
	"sync"

	"gitlab.com/thedahv/schemadex/pkg/crawler"
	"gitlab.com/thedahv/schemadex/pkg/db"
)

// Manager creates, supervises, and reports on crawls in progress. Ideally, a
// crawl can run from anywhere, reporting data, issues, and progress back to
// clients through the manager.
//
// The Manager is responsible for creating Crawlers and consequently clients do
// not get direct interaction with Crawler creation. When you create a Manager,
// you will likely want to set the CreateOptions field which is a function that
// generates a list crawler.Option for a Crawler. If supplied, it is called at
// crawl registration time with the root URL for the crawl. This is useful for
// any crawl options that are URL-dependent.
//
// If the CreateOptions field is not set, the manager creates vanilla crawls
// with the default crawler options. See the crawler package for more
// information on crawler behavior.
type Manager struct {
	crawls        map[url.URL]*crawler.Driver
	crawlsLock    sync.RWMutex
	db            db.Service
	options       []crawler.Option
	CreateOptions func(url url.URL) []crawler.Option
}

// New creates a new Manager configured to communicate with the given database
// service for tracking crawls and persisting crawl results.
//
// db can be nil the manager will continue to operate, but crawl results will
// not persist.
func New(db db.Service) *Manager {
	return &Manager{
		crawls: make(map[url.URL]*crawler.Driver),
		db:     db,
	}
}

// Add creates a new crawl driver for the given URL, adds it to the supervisor
// table, and starts the crawl.
func (m *Manager) Add(u url.URL) error {
	var options []crawler.Option
	if m.CreateOptions != nil {
		options = m.CreateOptions(u)
	}

	d, err := crawler.New(u, options...)
	if err != nil {
		return err
	}
	m.crawlsLock.Lock()
	m.crawls[u] = d
	m.crawlsLock.Unlock()

	// The crawl is run, managed, monitored for results and errors, and cleaned up
	// on completion all from within this goroutine.
	go m.runCrawl(u, d)

	return nil
}

// runCrawl kicks off a configured crawl driver, monitors and persists results
// from its operation, and cleans it up from supervision when it is finished.
func (m *Manager) runCrawl(src url.URL, driver *crawler.Driver) {
	log.SetPrefix("[Manager] ")
	schema, errors := driver.Start()
	for {
		if schema == nil && errors == nil {
			break
		}

		select {
		case s, ok := <-schema:
			if !ok {
				schema = nil
				continue
			}

			if m.db == nil {
				continue
			}

			// TODO batch schemas to cut down on DB IO wait time
			_, err := m.db.Create(s)
			if err != nil {
				if _, err := fmt.Fprintf(os.Stderr, "unable to save schema: '%s': %v",
					s.Key(), err); err != nil {
					log.Printf("issue printing errot to stderr: %v", err)
				}
			}
		case e, ok := <-errors:
			if !ok {
				errors = nil
				continue
			}
			if _, err := fmt.Fprintf(os.Stderr, "crawl error: %v\n", e); err != nil {
				log.Fatalf("unable to report error: %v", err)
			}
		}
	}

	log.Printf("crawl complete: %s\n", src.String())
	m.crawlsLock.Lock()
	delete(m.crawls, src)
	m.crawlsLock.Unlock()
}

// CrawlReport indicates how many links are in queue for the root domain of each
// crawl still in-flight.
type CrawlReport struct {
	URL   string `json:"url"`
	Depth int    `json:"depth"`
}

// Crawls reports the URL and the amount in queue of the remaining crawls
// in-flight
func (m *Manager) Crawls() []CrawlReport {
	m.crawlsLock.RLock()
	defer m.crawlsLock.RUnlock()

	var crawls []CrawlReport
	for u, crawl := range m.crawls {
		crawls = append(crawls, CrawlReport{URL: u.String(), Depth: crawl.Depth()})
	}

	return crawls
}

// Report indicates the link queue depth of a crawl identified by the root
// URL that started that crawl, if it exists and is in-flight. The second "ok"
// return parameter indicates whether the given crawl is under management at
// call time.
func (m *Manager) Report(u url.URL) (CrawlReport, bool) {
	m.crawlsLock.RLock()
	defer m.crawlsLock.RUnlock()

	var report CrawlReport
	crawl, ok := m.crawls[u]
	if !ok {
		return report, ok
	}

	return CrawlReport{URL: u.String(), Depth: crawl.Depth()}, true
}
