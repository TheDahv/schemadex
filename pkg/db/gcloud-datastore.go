package db

import (
	"fmt"

	"cloud.google.com/go/datastore"
	"gitlab.com/thedahv/schemadex/pkg/schema"
	"golang.org/x/net/context"
	"google.golang.org/api/iterator"
)

// GcloudDatastore implements the db.Service interface and connects to the
// Gcloud Datastore service to power data services for Schemadex.
type GcloudDatastore struct {
	kind      string
	projectID string
	client    *datastore.Client
	ctx       context.Context
}

// documentKind is the Entity Name in Google Cloud Datastore
const documentKind = "Schema"

// NewGcloudDatastore creates an instance configured to talk to the given Google
// project identified by the project ID. It assumes the proper environment
// variables have been set.
// See
// https://cloud.google.com/datastore/docs/reference/libraries#client-libraries-install-go
func NewGcloudDatastore(projectID string) (*GcloudDatastore, error) {
	if projectID == "" {
		return nil, fmt.Errorf("projectID cannot be empty")
	}

	client, ctx, err := getClient(projectID)
	if err != nil {
		return nil, fmt.Errorf("GcloudDatastore.New: %v", err)
	}

	store := &GcloudDatastore{
		kind:      documentKind,
		projectID: projectID,
		client:    client,
		ctx:       ctx,
	}
	return store, nil
}

// getClient is useful boilerplate for all queries and statements against the
// remote datastore.
func getClient(projectID string) (*datastore.Client, context.Context, error) {
	ctx := context.Background()
	client, err := datastore.NewClient(ctx, projectID)
	if err != nil {
		return nil, ctx, fmt.Errorf("could not create google datastore client: %v", err)
	}

	return client, ctx, nil
}

// Create creates a new Schema in Google Cloud Datastore and returns its
// document ID.
func (g *GcloudDatastore) Create(schema schema.Schema) (EntryID, error) {
	// Note, schemaKey has a Namespace property that could be useful for
	// partitioning. No need for it right now, though.
	schemaKey := datastore.NameKey(g.kind, schema.Key(), nil)

	dbs := convert(schema)
	id, err := g.client.Put(g.ctx, schemaKey, &dbs)
	if err != nil {
		return EntryID(""), err
	}

	return EntryID(id.String()), nil
}

// Search queries Google Cloud Datastore for the current project to find any
// Schema Entities matching the given search config
func (g *GcloudDatastore) Search(cfg SearchConfig) ([]schema.Schema, error) {
	q := datastore.NewQuery(g.kind)
	if cfg.DomainURL != "" {
		q = q.Filter("DomainURL =", cfg.DomainURL)
	}
	if cfg.Type != "" {
		q = q.Filter("Type =", cfg.Type)
	}

	// TODO move pagination to SearchConfig
	q = q.Limit(50)

	var results []schema.Schema
	ctx := context.Background()
	for c := g.client.Run(ctx, q); ; {
		var schema dbSchema
		if _, err := c.Next(&schema); err != nil {
			if err == iterator.Done {
				break
			} else {
				return results, err
			}
		}

		s, err := parse(schema)
		if err != nil {
			return results,
				fmt.Errorf("could not parse database result to schema: %v", err)
		}
		results = append(results, s)
	}

	return results, nil
}

// Counts computes aggregate counts for all Schema entities matching the given
// search.
func (g *GcloudDatastore) Counts(cfg SearchConfig) (CountReport, error) {
	q := datastore.NewQuery(g.kind).KeysOnly()
	if cfg.DomainURL != "" {
		q = q.Filter("DomainURL =", cfg.DomainURL)
	}
	if cfg.Type != "" {
		q = q.Filter("Type =", cfg.Type)
	}

	var counts CountReport

	ctx := context.Background()
	schemas, err := g.client.Count(ctx, q)
	if err != nil {
		return counts, err
	}
	counts.Schemas = schemas

	return counts, nil
}

// dbSchema exists to convert any unsupported fields into versions acceptable by
// Google Cloud Datastore
type dbSchema struct {
	schema.Schema
	Implementation string
}

// convert converts fields from the vanilla Schema type to the dbSchema type
func convert(s schema.Schema) dbSchema {
	return dbSchema{Schema: s, Implementation: s.Implementation.String()}
}

func parse(db dbSchema) (schema.Schema, error) {
	var s schema.Schema
	i, err := schema.ParseImplementation(db.Implementation)
	if err != nil {
		return s, fmt.Errorf("could not parse implementation: %v", err)
	}

	s.Type = db.Type
	s.Implementation = i
	s.VocabularyHost = db.VocabularyHost
	s.PageURL = db.PageURL
	s.DomainURL = db.DomainURL
	s.CrawledAt = db.CrawledAt

	return s, err
}
