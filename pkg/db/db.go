package db

import (
	"gitlab.com/thedahv/schemadex/pkg/schema"
)

// EntryID identifies a schema stored in the database
type EntryID string

// Service defines the behavior of the data store service accessible to other
// packages of the schemedex domain.
type Service interface {
	Create(schema.Schema) (EntryID, error)
	Search(SearchConfig) ([]schema.Schema, error)
	Counts(SearchConfig) (CountReport, error)
}

// SearchConfig contains search query parameters, results limits, and any other
// client-facing concerns useful for building a query.
type SearchConfig struct {
	Type      string
	DomainURL string
}

// CountReport stores aggregate counts on Schema matching the given query, or
// across all stored results if the SearchConfig is empty.
type CountReport struct {
	Schemas   int `json:"schema"`
	Domains   int `json:"domains"`
	Pages     int `json:"pages"`
	Microdata int `json:"microdata"`
	JSONLD    int `json:"jsonld"`
}
