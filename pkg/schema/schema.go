package schema

import (
	"crypto/md5"
	"fmt"
	"io"
	"time"
)

// Schema models usage of Schema.org markup encountered during the crawl of a
// site.
type Schema struct {
	Type           string
	Implementation Implementation
	VocabularyHost string
	PageURL        string
	DomainURL      string
	CrawledAt      time.Time
}

// Implementation indicates which approach a page used to mark up Schema.org
// data.
type Implementation uint8

// Enumerates the known implementations of marking up a page with Schema.org
// data.
const (
	Microdata Implementation = iota
	JSONLD
)

// ParseImplementation reverses a string back to the package-definition of these
// constant values.
func ParseImplementation(s string) (Implementation, error) {
	var i Implementation
	switch s {
	case "microdata":
		return Microdata, nil
	case "json+ld":
		return JSONLD, nil
	default:
		return i, fmt.Errorf("unrecognized implementation: %s", s)
	}
}

func (i Implementation) String() string {
	var s string
	switch i {
	case Microdata:
		s = "microdata"
	case JSONLD:
		s = "json+ld"
	}

	return s
}

// Key hashes properties of the schema to create an identifier for an instance
// found on a given website.
func (s Schema) Key() string {
	h := md5.New()

	io.WriteString(h, s.Type)
	io.WriteString(h, s.Implementation.String())
	io.WriteString(h, s.PageURL)

	return fmt.Sprintf("%x", h.Sum(nil))
}
