package main

import (
	"fmt"
	"html"
)

// GoogleStructuredHTMLLink returns a link to Google's Structured Data testing
// tool pointed at the given URL .
func GoogleStructuredHTMLLink(url string) string {
	return fmt.Sprintf(
		"https://search.google.com/structured-data/testing-tool/u/0/#url=%s",
		html.EscapeString(url),
	)
}
