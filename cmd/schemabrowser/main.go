package main

import (
	"flag"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/thedahv/schemadex/pkg/db"
)

func main() {
	var port = flag.Int("port", 8080, "port on which to bind the server (8080)")
	var projectID = flag.String("projectID", "", "the Google Cloud project ID")
	flag.Parse()

	gdb, err := db.NewGcloudDatastore(*projectID)
	if err != nil {
		log.Fatalf("could not create client: %v", err)
	}

	templates, err := prepareTemplates()
	if err != nil {
		log.Fatalf("issue setting up templates: %v", err)
	}

	svr := &Server{db: gdb, port: *port, templates: templates}

	log.Fatal(svr.Start())
}

// Server listens for requests to add new crawls and reports on progress
type Server struct {
	db        db.Service
	port      int
	templates map[string]*template.Template
}

// Start binds the server to the configured port and listens for traffic.
func (s *Server) Start() error {
	log.SetPrefix("[schemabrowser] ")
	mux := http.NewServeMux()

	cwd, err := os.Getwd()
	if err != nil {
		return fmt.Errorf("unable to get current directory for assets: %v", err)
	}

	fs := http.StripPrefix("/css", http.FileServer(http.Dir(cwd+"/assets/css")))
	mux.Handle("/css/", fs)
	mux.HandleFunc("/api/", s.HandleAPI)
	mux.HandleFunc("/", s.HandleApp)

	log.Printf("listening on :%d\n", s.port)
	return http.ListenAndServe(fmt.Sprintf(":%d", s.port), mux)
}

// HandleApp serves routes for the main web applications
func (s *Server) HandleApp(w http.ResponseWriter, r *http.Request) {
	log.SetPrefix("[schemabrowser/App] ")
	log.Println(r.URL.Path)

	var err error
	switch r.URL.Path {
	case "/":
		err = serveHome(s, w, r)
		log.Printf("[/]: %v\n", err)
	case "/about":
		err = serveStatic("about")(s, w, r)
		log.Printf("[/about]: %v\n", err)
	default:
		w.WriteHeader(http.StatusNotFound)
		if _, err := w.Write([]byte("not found")); err != nil {
			log.Printf("unable to serve 404: %v\n", err)
		}
		return
	}

	if err != nil {
		// TODO: Serve a real Oops page
		w.WriteHeader(http.StatusInternalServerError)
		if _, err := w.Write([]byte("done gone busted it")); err != nil {
			log.Printf("unable to serve server error page")
		}
		return
	}
}

// HandleAPI serves routes that implement the exposed API
func (s *Server) HandleAPI(w http.ResponseWriter, r *http.Request) {
	log.SetPrefix("[schemabrowser/API] ")
	log.Printf(r.URL.Path)
	if r.Method == "GET" {
		switch r.URL.Path {
		case "/api/crawls/counts":
			if err := generateCounts(s, w, r); err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				if _, err := w.Write([]byte("unable to generate counts please try again")); err != nil {
					log.Printf("GET /api/crawls/counts: unable to write response: %v", err)
				}
				log.Printf("unable to generate counts: %v\n", err)
				return
			}
		}
	}
}

func prepareTemplates() (map[string]*template.Template, error) {
	templates := make(map[string]*template.Template)

	helpers := template.FuncMap{
		"GoogleStructuredHTMLLink": GoogleStructuredHTMLLink,
	}

	dir, err := os.Getwd()
	if err != nil {
		return templates, fmt.Errorf("unable to determine template directory: %v", err)
	}

	mt, err := template.New("main").Parse(
		`{{define "main"}}{{template "base" .}}{{end}}`,
	)
	if err != nil {
		return templates, fmt.Errorf("unable to parse base template: %v", err)
	}

	mt, err = mt.ParseFiles(dir + "/templates/layouts/main.html.tmpl")
	if err != nil {
		return templates, fmt.Errorf("could not load template from disk: %v", err)
	}

	layouts, err := filepath.Glob(dir + "/templates/layouts/*.tmpl")
	if err != nil {
		return templates, fmt.Errorf("cannot load layouts: %v", err)
	}
	pages, err := filepath.Glob(dir + "/templates/pages/*.tmpl")
	if err != nil {
		return templates, fmt.Errorf("cannot load pages: %v", err)
	}

	for _, page := range pages {
		name := strings.Replace(filepath.Base(page), ".html.tmpl", "", 1)
		c, err := mt.Lookup("base").Clone()
		if err != nil {
			return templates, fmt.Errorf("could not clone base template for %s: %v", page, err)
		}
		files := append(layouts, page)
		templates[name], err = c.Funcs(helpers).ParseFiles(files...)
		if err != nil {
			return templates, fmt.Errorf("%s unable to parse files: %v", name, err)
		}
	}

	return templates, nil
}
