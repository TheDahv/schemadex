package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/thedahv/schemadex/pkg/db"
	"gitlab.com/thedahv/schemadex/pkg/schema"
)

// routeHandler describes a function that can implement the handler for a given
// route in the application server.
//
// routeHandlers are expected to write to and close the response on a successful
// run.
//
// routeHandlers DO NOT respond to their clients on error, and instead return
// an error to be handled closer to the http.Mux level.
type routeHandler func(*Server, http.ResponseWriter, *http.Request) error

// serveStatic returns a routeHandler that will load a template identified by
// name and serve that template content to the response.
func serveStatic(name string) routeHandler {
	return func(s *Server, w http.ResponseWriter, r *http.Request) error {
		t, ok := s.templates[name]
		if !ok {
			return fmt.Errorf("no template for %s", name)
		}

		if err := t.Execute(w, nil); err != nil {
			return fmt.Errorf("error rendering %s: %v", name, err)
		}

		return nil
	}
}

// serveHome loads Schema information and serves it on the home page. It
// supports possible presence of query parameters describing a more specific
// search for Schema results.
func serveHome(s *Server, w http.ResponseWriter, r *http.Request) error {
	if err := r.ParseForm(); err != nil {
		return fmt.Errorf("unable to parse query params: %v", err)
	}

	query := db.SearchConfig{
		Type:      r.Form.Get("type"),
		DomainURL: r.Form.Get("domainURL"),
	}

	var results []schema.Schema
	var err error
	if results, err = s.db.Search(query); err != nil {
		return fmt.Errorf("unable to search schema: %v", err)
	}
	data := struct {
		Query   db.SearchConfig
		Results []schema.Schema
	}{
		Query:   query,
		Results: results,
	}

	t := s.templates["index"]

	if err := t.Execute(w, data); err != nil {
		return fmt.Errorf("unable to render home page: %v", err)
	}

	return nil
}

// generateCounts returns a JSON response for aggregate count information for
// Schema matching a given query.
func generateCounts(s *Server, w http.ResponseWriter, r *http.Request) error {
	if err := r.ParseForm(); err != nil {
		return fmt.Errorf("unable to parse query: %v", err)
	}

	query := db.SearchConfig{
		Type:      r.Form.Get("type"),
		DomainURL: r.Form.Get("domainURL"),
	}

	report, err := s.db.Counts(query)
	if err != nil {
		return fmt.Errorf("unable to get counts from db: %v", err)
	}

	out, err := json.Marshal(report)
	if err != nil {
		return fmt.Errorf("unable to marshal JSON: %v", err)
	}

	_, err = w.Write(out)
	return err
}
