package main

import (
	"fmt"
	"log"
	"net/url"
	"os"

	"gitlab.com/thedahv/schemadex/pkg/crawler"
	"gitlab.com/thedahv/schemadex/pkg/db"
)

func main() {
	args := os.Args[1:]

	if len(args) < 2 {
		log.Fatal(usage())
	}

	projectID := args[0]
	srcURL := args[1]

	gdb, err := db.NewGcloudDatastore(projectID)
	if err != nil {
		log.Fatalf("could not create client: %v", err)
	}

	src, err := url.Parse(srcURL)
	if err != nil {
		log.Fatalf("couldn't parse source URL: %v", err)
	}

	d, err := crawler.New(
		*src,
		crawler.WithConcurrency(15),
		crawler.WithFilter(
			crawler.FilterCompose(
				crawler.FilterHTTPOnly,
				crawler.FilterInternalOnly(*src),
			),
		),
	)

	if err != nil {
		log.Fatalf("error setting up crawler: %v", err)
	}

	schema, errors := d.Start()
	for {
		if schema == nil && errors == nil {
			break
		}

		select {
		case s, ok := <-schema:
			if !ok {
				schema = nil
				continue
			}

			key, err := gdb.Create(s)
			if err != nil {
				fmt.Fprintf(os.Stdout, "unable to save schema: '%s': %v",
					s.Key(), err)
			} else {
				fmt.Println("saved", key)
			}
		case e, ok := <-errors:
			if !ok {
				errors = nil
				continue
			}
			if _, err := fmt.Fprintf(os.Stderr, "crawl error: %v\n", e); err != nil {
				log.Fatalf("unable to report error: %v", err)
			}
		}
	}
}

func usage() string {
	return "crawlschemas <projectid> <url>"
}
