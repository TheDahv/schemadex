package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/url"

	"gitlab.com/thedahv/schemadex/pkg/crawler"
	"gitlab.com/thedahv/schemadex/pkg/db"
	"gitlab.com/thedahv/schemadex/pkg/manager"
)

func main() {
	var port = flag.Int("port", 3000, "port on which to bind the server (3000)")
	var projectID = flag.String("projectID", "", "the Google Cloud project ID")
	flag.Parse()

	gdb, err := db.NewGcloudDatastore(*projectID)
	if err != nil {
		log.Fatalf("could not create client: %v", err)
	}

	m := manager.New(gdb)
	m.CreateOptions = func(u url.URL) []crawler.Option {
		return []crawler.Option{
			crawler.WithConcurrency(15),
			crawler.WithClient(&crawlclient{}),
			crawler.WithFilter(
				crawler.FilterCompose(
					crawler.FilterHTTPOnly,
					crawler.FilterInternalOnly(u),
				),
			),
		}
	}

	server := Server{
		manager: m,
		port:    *port,
	}

	log.Fatal(server.Start())
}

// Server listens for requests to add new crawls and reports on progress
type Server struct {
	manager *manager.Manager
	port    int
}

// Start binds the server to the configured port and listens for traffic
func (s Server) Start() error {
	mux := http.NewServeMux()
	mux.HandleFunc("/crawls", s.HandleCrawls)

	return http.ListenAndServe(fmt.Sprintf(":%d", s.port), mux)
}

// HandleCrawls manages all routes related to crawl management and reporting:
//
//		GET /crawls - Reports all currently running crawls
//
//		POST /crawls - Registers a new crawl
//		- Query Parameters:
//			- url: url-encoded URL to begin crawling
func (s Server) HandleCrawls(w http.ResponseWriter, r *http.Request) {
	log.SetPrefix("[HandleCrawls] ")
	if err := r.ParseForm(); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Printf("unable to parse form: %v", err)
		return
	}

	if r.URL.Path == "/crawls" && r.Method == "POST" {
		u, err := url.QueryUnescape(r.Form.Get("url"))
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			if _, err := w.Write([]byte("bad URL, please try again")); err != nil {
				log.Printf("POST /crawls: unable to write response: %v", err)
			}
			return
		}

		if len(u) == 0 {
			w.WriteHeader(http.StatusBadRequest)
			if _, err := w.Write([]byte("url cannot be empty")); err != nil {
				log.Printf("POST /crawls: unable to write response: %v", err)
			}
			return
		}

		if err := s.addCrawl(u); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			// TODO user-facing error
			log.Printf("unable to add crawl: %v", err)
		}
		w.WriteHeader(http.StatusOK)
		if _, err := w.Write([]byte(fmt.Sprintf("crawl started for %s", u))); err != nil {
			log.Printf("POST /crawls: unable to write response: %v", err)
		}

		return
	}

	if r.URL.Path == "/crawls" && r.Method == "GET" {
		out, err := json.Marshal(s.manager.Crawls())
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			if _, err := w.Write([]byte("Unable to generate report. Please try again")); err != nil {
				log.Printf("GET /crawls: unable to write response: %v", err)
			}
			log.Printf("unable to format crawl report: %v", err)
			return
		}

		w.WriteHeader(http.StatusOK)
		if _, err := w.Write(out); err != nil {
			log.Printf("GET /crawls: unable to write response: %v", err)
		}
		return
	}

	w.WriteHeader(http.StatusNotFound)
}

func (s Server) addCrawl(rootURL string) error {
	u, err := url.Parse(rootURL)
	if err != nil {
		return err
	}

	return s.manager.Add(*u)
}

// crawlclient implements an extension to the default http.Client with the
// addition of a User-Agent identifying the crawler as schemadex.
type crawlclient struct {
	http.Client
}

// Get issues a request for the page identified by URL and returns the response
// or error. It adds a customer user-agent for any sites that reject anonymous
// traffic.
func (cc *crawlclient) Get(url string) (*http.Response, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, fmt.Errorf("unable to create request: %v", err)
	}

	req.Header.Add("User-Agent", "schemadex-crawl")
	return cc.Do(req)
}
