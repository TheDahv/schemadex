#!/usr/bin/env bash

# NOTE: Please ensure the `GOOGLE_APPLICATION_CREDENTIALS` environment variable
# is set in the current environment. It should contain the path to the current
# 'gcloud-creds.json' file for the project.

# This file helps start the dev database:
# https://cloud.google.com/datastore/docs/tools/datastore-emulator

case "$1" in
  start)
    gcloud beta emulators datastore start & &>/dev/null
    ;;

  set-env)
    gcloud beta emulators datastore env-init
    ;;

  stop)
    ps ax | grep datastore | grep gcloud | awk '{print $1}'| xargs kill;
    gcloud beta emulators datastore env-unset
    ;;

  *)
    echo "Usage: ./dev-setup.sh (start|set-env|stop)"
    exit 1
esac
